package com.goose;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.PrintStream;
import java.nio.CharBuffer;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.goose.util.Dice;

public class TestGooseGame {

    private static class InstructibleReader implements Readable {
        private String input = "";

        @Override
        public int read(CharBuffer cb) throws IOException {
            while (input.isEmpty()) {
                try {
                    Thread.sleep(30);
                } catch (InterruptedException e) {
                    return 0;
                }
            }
            cb.append(input);
            int l = input.getBytes().length;
            input = "";
            return l;
        }

        public void setInput(String input) {
            this.input = input + "\r\n";
        }
    }


    @Test
    public void testScanner() throws IOException {
        PipedInputStream snk = new PipedInputStream();
        try (PipedOutputStream pos = new PipedOutputStream(snk)) {
            BufferedReader bf = new BufferedReader(new InputStreamReader(snk));
            bf.mark(0);
            PrintStream printer = new PrintStream(pos);
            InstructibleReader in = new InstructibleReader();
            Runnable r = new Runnable() {
                @Override
                public void run() {
                    Scanner scanner = new Scanner(in);
                    String s = "";
                    while(!"exit".equals(s)) {
                        s = scanner.nextLine();
                        printer.println("we got it!");
                    }
                    scanner.close();
                }
            };
            Thread t = new Thread(r, "GooseGameThread");
            t.start();
            String response = "";
            in.setInput("first line passed ");
            response = bf.readLine();
            assertEquals("we got it!", response);

            in.setInput("secoond line passed ");
            response = bf.readLine();
            assertEquals("we got it!", response);

            in.setInput("third line passed ");
            response = bf.readLine();
            assertEquals("we got it!", response);

            t.interrupt();
            printer.close();
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    // @formatter:off
    /*
     * 1. Add Player If there is no participant the user writes: "add player Pippo"
     * the system responds: "players: Pippo" the user writes: "add player Pluto" the
     * system responds: "players: Pippo, Pluto"
     */

    @Test
    public void testAddPlayer() throws IOException {
        PipedInputStream snk = new PipedInputStream();
        try (PipedOutputStream pos = new PipedOutputStream(snk)) {
            BufferedReader bf = new BufferedReader(new InputStreamReader(snk));
            bf.mark(0);
            PrintStream printer = new PrintStream(pos);

            InstructibleReader in = new InstructibleReader();
            Runnable r = new Runnable() {
                @Override
                public void run() {
                    Scanner scanner = new Scanner(in);
                    GooseGame game = new GooseGame(printer, scanner);
                    game.play();
                }
            };
            Thread t = new Thread(r, "GooseGameThread");
            t.start();
            Thread.sleep(300);
            spinOutIrrelevantContent(snk);
            in.setInput("add player Pippo");
            String response = bf.readLine();
            assertEquals("players: Pippo", response);
            spinOutBoardDisplay(bf);

            in.setInput("add player Pluto");
            response = bf.readLine();
            assertEquals("players: Pippo, Pluto", response);

            in.setInput("exit\n");
            t.join(300);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }


    /*
     * 2. Duplicated Player If there is already a participant "Pippo" the user
     * writes: "add player Pippo" the system responds:
     * "Pippo: already existing player"
     **/


    @Test
    public void testAddDuplicatedPlayer() throws IOException {
        PipedInputStream snk = new PipedInputStream();
        try (PipedOutputStream pos = new PipedOutputStream(snk)) {
            BufferedReader bf = new BufferedReader(new InputStreamReader(snk));
            PrintStream printer = new PrintStream(pos);

            InstructibleReader in = new InstructibleReader();
            Runnable r = new Runnable() {
                @Override
                public void run() {
                    Scanner scanner = new Scanner(in);
                    GooseGame game = new GooseGame(printer, scanner);
                    game.play();
                }
            };
            Thread t = new Thread(r, "GooseGameThread");
            t.start();
            Thread.sleep(300);
            // spin out content not needed
            spinOutIrrelevantContent(snk);
            in.setInput("add player Pippo");
            //Thread.sleep(READ_WAIT_TIME);
            String response = bf.readLine();
            assertEquals("players: Pippo", response);
            spinOutBoardDisplay(bf);

            in.setInput("add player Pippo");
            Thread.sleep(1000);
            response = bf.readLine();
            assertEquals("Pippo: already existing player", response);

            in.setInput("exit\n");
            t.join(300);
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    /* *
     * 1. Start If there are two participants "Pippo" and "Pluto" on space "Start"
     * the user writes: "move Pippo 6, 1" the system responds: "Pippo rolls 6, 1. Pippo moves from Start to 7"
     * the user writes: "move Pluto 2, 2" the system responds: "Pluto rolls 2, 2. Pluto moves from Start to 4"
     * the user writes: "move Pippo 2, 3" the system responds: "Pippo rolls 2, 3. Pippo moves from 7 to 12"
     *
     *  Original testcase were modified, beacuse map objects interfere at 6 and 5
     */

    @Test
    public void testMovePlayerManually() throws IOException {
        PipedInputStream snk = new PipedInputStream();
        try (PipedOutputStream pos = new PipedOutputStream(snk)) {
            BufferedReader bf = new BufferedReader(new InputStreamReader(snk));
            PrintStream printer = new PrintStream(pos);

            InstructibleReader in = new InstructibleReader();
            Runnable r = new Runnable() {
                @Override
                public void run() {
                    Scanner scanner = new Scanner(in);
                    GooseGame game = new GooseGame(printer, scanner);
                    game.play();
                }
            };
            Thread t = new Thread(r, "GooseGameThread");
            t.start();
            Thread.sleep(300);
            spinOutIrrelevantContent(snk);
            in.setInput("add player Pippo");
            String response = bf.readLine();
            assertEquals("players: Pippo", response);

            spinOutBoardDisplay(bf);
            in.setInput("add player Pluto");
            response = bf.readLine();
            assertEquals("players: Pippo, Pluto", response);

            spinOutBoardDisplay(bf);
            in.setInput("move Pippo 6, 1");
            response = bf.readLine();
            assertEquals("Pippo rolls 6, 1. Pippo moves from The Start to 7", response);

            spinOutBoardDisplay(bf);
            in.setInput("move Pluto 2, 2");
            response = bf.readLine();
            assertEquals("Pluto rolls 2, 2. Pluto moves from The Start to 4", response);

            spinOutBoardDisplay(bf);
            in.setInput("move Pippo 2, 3");
            response = bf.readLine();
            assertEquals("Pippo rolls 2, 3. Pippo moves from 7 to 12", response);

            in.setInput("exit\n");
            t.join(300);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }


    /* ### 3. Win As a player, I win the game if I land on space "63"
     *
     *
     * 1. Victory If there is one participant "Pippo" on space "60" the user writes:
     * "move Pippo 1, 2" the system responds:
     * "Pippo rolls 1, 2. Pippo moves from 60 to 63. Pippo Wins!!"
     */

    @Test
    public void testMoveAndWin() throws IOException {
        PipedInputStream snk = new PipedInputStream();
        try (PipedOutputStream pos = new PipedOutputStream(snk)) {
            BufferedReader bf = new BufferedReader(new InputStreamReader(snk));
            PrintStream printer = new PrintStream(pos);

            InstructibleReader in = new InstructibleReader();
            Scanner scanner = new Scanner(in);
            GooseGame game = new GooseGame(printer, scanner);

            Map<String, Player> players = new HashMap<String, Player>();
            Board board = new Board(new int[]{}, new int[]{}, printer);
            Player pippo = new Player("Pippo");
            pippo.setPosition(board.get(60));
            players.put("Pippo", pippo);
            boolean running = game.movePlayer(board, players, Mockito.mock(Dice.class), "move Pippo 1, 2");
            assertFalse("Game should not continue" , running);
            assertTrue("Player should win" , pippo.hasWon());
            assertEquals("Pippo rolls 1, 2. Pippo moves from 60 to The Goal. Pippo Wins!!" , bf.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /* 2. Winning with the exact dice shooting If there is one participant "Pippo"
     * on space "60" the user writes: "move Pippo 3, 2" the system responds:
     * "Pippo rolls 3, 2. Pippo moves from 60 to 63. Pippo bounces! Pippo returns to 61"
     * ### 4. The game throws the dice As a player, I want the game throws the dice
     * for me to save effort
     */

    @Test
    public void testMoveAndBounce() throws IOException {
        PipedInputStream snk = new PipedInputStream();
        try (PipedOutputStream pos = new PipedOutputStream(snk)) {
            BufferedReader bf = new BufferedReader(new InputStreamReader(snk));
            PrintStream printer = new PrintStream(pos);

            InstructibleReader in = new InstructibleReader();
            Scanner scanner = new Scanner(in);
            GooseGame game = new GooseGame(printer, scanner);

            Map<String, Player> players = new HashMap<String, Player>();
            Board board = new Board(new int[]{}, new int[]{}, printer);
            Player pippo = new Player("Pippo");
            pippo.setPosition(board.get(60));
            players.put("Pippo", pippo);
            boolean running = game.movePlayer(board, players, Mockito.mock(Dice.class), "move Pippo 3, 2");
            assertTrue("Game should continue" , running);
            assertFalse("Player should not win" , pippo.hasWon());
            assertEquals("Pippo rolls 3, 2. Pippo moves from 60 to 63. Pippo bounces! Pippo returns to 61" , bf.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /*
     * 1. Dice roll If there is one participant "Pippo" on space "4" assuming that
     * the dice get 1 and 2 when the user writes: "move Pippo" the system responds:
     * "Pippo rolls 1, 2. Pippo moves from 4 to 7"
     */

    @Test
    public void testAutoDiceMove() throws IOException {
        PipedInputStream snk = new PipedInputStream();
        try (PipedOutputStream pos = new PipedOutputStream(snk)) {
            BufferedReader bf = new BufferedReader(new InputStreamReader(snk));
            PrintStream printer = new PrintStream(pos);

            InstructibleReader in = new InstructibleReader();
            Scanner scanner = new Scanner(in);
            GooseGame game = new GooseGame(printer, scanner);

            Map<String, Player> players = new HashMap<String, Player>();
            Board board = new Board(new int[]{}, new int[]{}, printer);
            Player pippo = new Player("Pippo");
            pippo.setPosition(board.get(4));
            players.put("Pippo", pippo);
            Dice dice = Mockito.mock(Dice.class);

            Mockito.when(dice.throwSingleDice()).thenAnswer(new Answer<Integer>() {
                private int callCounter = 0;

                @Override
                public Integer answer(InvocationOnMock invocation) throws Throwable {
                    return callCounter++ == 0 ? 1 : 2;
                }

            });
            game.movePlayer(board, players, dice, "move Pippo");
            assertEquals("Pippo rolls 1, 2. Pippo moves from 4 to 7" , bf.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /* ### 5. Space "6" is "The Bridge" As a player, when I get to the space
     * "The Bridge", I jump to the space "12"
     * 1. Get to "The Bridge" If there is one participant "Pippo" on space "4"
     * assuming that the dice get 1 and 1 when the user writes: "move Pippo" the
     * system responds:
     * "Pippo rolls 1, 1. Pippo moves from 4 to The Bridge. Pippo jumps to 12"
     */


    @Test
    public void testBridgeMove() throws IOException {
        PipedInputStream snk = new PipedInputStream();
        try (PipedOutputStream pos = new PipedOutputStream(snk)) {
            BufferedReader bf = new BufferedReader(new InputStreamReader(snk));
            PrintStream printer = new PrintStream(pos);

            InstructibleReader in = new InstructibleReader();
            Scanner scanner = new Scanner(in);
            GooseGame game = new GooseGame(printer, scanner);

            Map<String, Player> players = new HashMap<String, Player>();
            Board board = new Board(new int[]{}, new int[]{6}, printer);
            Player pippo = new Player("Pippo");
            pippo.setPosition(board.get(4));
            players.put("Pippo", pippo);
            Dice dice = Mockito.mock(Dice.class);

            Mockito.when(dice.throwSingleDice()).thenReturn(1);
            game.movePlayer(board, players, dice, "move Pippo");
            assertEquals("Pippo rolls 1, 1. Pippo moves from 4 to The Bridge. Pippo jumps to 12" , bf.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /* ### 6. If you land on "The Goose", move again As a player, when I get to a
     * space with a picture of "The Goose", I move forward again by the sum of the
     * two dice rolled before
     *
     * The spaces 5, 9, 14, 18, 23, 27 have a picture of "The Goose"
     *
     *
     * 1. Single Jump If there is one participant "Pippo" on space "3" assuming that
     * the dice get 1 and 1 when the user writes: "move Pippo" the system responds:
     * "Pippo rolls 1, 1. Pippo moves from 3 to 5, The Goose. Pippo moves again and goes to 7"
     */


    @Test
    public void testGooseMove() throws IOException {
        PipedInputStream snk = new PipedInputStream();
        try (PipedOutputStream pos = new PipedOutputStream(snk)) {
            BufferedReader bf = new BufferedReader(new InputStreamReader(snk));
            PrintStream printer = new PrintStream(pos);

            InstructibleReader in = new InstructibleReader();
            Scanner scanner = new Scanner(in);
            GooseGame game = new GooseGame(printer, scanner);

            Map<String, Player> players = new HashMap<String, Player>();
            Board board = new Board(new int[]{5,9,14,18,23,27}, new int[]{}, printer);
            Player pippo = new Player("Pippo");
            pippo.setPosition(board.get(3));
            players.put("Pippo", pippo);
            Dice dice = Mockito.mock(Dice.class);

            Mockito.when(dice.throwSingleDice()).thenReturn(1);
            game.movePlayer(board, players, dice, "move Pippo");
            assertEquals("Pippo rolls 1, 1. Pippo moves from 3 to 5, The Goose. Pippo moves again and goes to 7" , bf.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



    /* 2. Multiple Jump If there is one participant "Pippo" on space "10" assuming
     * that the dice get 2 and 2 when the user writes: "move Pippo" the system
     * responds:
     * "Pippo rolls 2, 2. Pippo moves from 10 to 14, The Goose. Pippo moves again and goes to 18, The Goose. Pippo moves again and goes to 22"
     *
     */

    @Test
    public void testMultipleGooseMove() throws IOException {
        PipedInputStream snk = new PipedInputStream();
        try (PipedOutputStream pos = new PipedOutputStream(snk)) {
            BufferedReader bf = new BufferedReader(new InputStreamReader(snk));
            PrintStream printer = new PrintStream(pos);

            InstructibleReader in = new InstructibleReader();
            Scanner scanner = new Scanner(in);
            GooseGame game = new GooseGame(printer, scanner);

            Map<String, Player> players = new HashMap<String, Player>();
            Board board = new Board(new int[]{5,9,14,18,23,27}, new int[]{}, printer);
            Player pippo = new Player("Pippo");
            pippo.setPosition(board.get(10));
            players.put("Pippo", pippo);
            Dice dice = Mockito.mock(Dice.class);

            Mockito.when(dice.throwSingleDice()).thenReturn(2);
            game.movePlayer(board, players, dice, "move Pippo");
            assertEquals("Pippo rolls 2, 2. Pippo moves from 10 to 14, The Goose. Pippo moves again and goes to 18, The Goose. Pippo moves again and goes to 22" , bf.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /*
     * ### 7. Prank (Optional Step) As a player, when I land on a space occupied by
     * another player, I send him to my previous position so that the game can be
     * more entertaining.

     * 1. Prank If there are two participants "Pippo" and "Pluto" respectively on
     * spaces "15" and "17" assuming that the dice get 1 and 1 when the user writes:
     * "move Pippo" the system responds:
     * "Pippo rolls 1, 1. Pippo moves from 15 to 17. On 17 there is Pluto, who returns to 15"
     *
     */
    @Test
    public void testPrankMove() throws IOException {
        PipedInputStream snk = new PipedInputStream();
        try (PipedOutputStream pos = new PipedOutputStream(snk)) {
            BufferedReader bf = new BufferedReader(new InputStreamReader(snk));
            PrintStream printer = new PrintStream(pos);

            InstructibleReader in = new InstructibleReader();
            Scanner scanner = new Scanner(in);
            GooseGame game = new GooseGame(printer, scanner);

            Map<String, Player> players = new HashMap<String, Player>();
            Board board = new Board(new int[]{5,9,14,18,23,27}, new int[]{}, printer);
            Player pippo = new Player("Pippo");
            pippo.setPosition(board.get(15));
            players.put("Pippo", pippo);

            {
                Player pluto = new Player("Pluto");
                pluto.setPosition(board.get(17));
                players.put(pluto.getName(), pluto);
            }

            Dice dice = Mockito.mock(Dice.class);
            Mockito.when(dice.throwSingleDice()).thenReturn(1);
            game.movePlayer(board, players, dice, "move Pippo");
            assertEquals("Pippo rolls 1, 1. Pippo moves from 15 to 17. On 17 there is Pluto, who returns to 15" , bf.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void spinOutBoardDisplay(BufferedReader bf) throws IOException {
        String s = "";
        while (!(s = bf.readLine()).endsWith("]"));
    }

    private void spinOutIrrelevantContent(PipedInputStream snk) throws IOException {
        while (snk.available() > 0) {
            snk.read();
        }
    }

}
