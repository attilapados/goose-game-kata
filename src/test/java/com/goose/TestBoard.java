package com.goose;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestBoard {

    @Test
    public void testMoveBounce() {
        Board b = new Board(new int[]{5,9,14,18,23,27}, new int[]{6}, System.out);
        Player player = new Player("Pippo");
        player.setPosition(b.get(61));
        int retVal = b.bouncedBackMove(player.getPosition().getId(), 3, player);
        assertEquals(61, retVal);
    }


    @Test
    public void testMoveBounceOnGoal() {
        Board b = new Board(new int[]{5,9,14,18,23,27}, new int[]{6}, System.out);
        Player player = new Player("Pippo");
        player.setPosition(b.get(61));
        int retVal = b.bouncedBackMove(player.getPosition().getId(), 2, player);
        assertEquals(63, retVal);
        assertEquals(Special.GOAL, b.get(retVal).getSpecial());
    }

    @Test
    public void testMoveNoBounce() {
        Board b = new Board(new int[]{5,9,14,18,23,27}, new int[]{6}, System.out);
        Player player = new Player("Pippo");
        player.setPosition(b.get(11));
        int retVal = b.bouncedBackMove(player.getPosition().getId(), 3, player);
        assertEquals(14, retVal);
    }


    @Test(expected=IllegalArgumentException.class)
    public void testMoveNegative() {
        Board b = new Board(new int[]{5,9,14,18,23,27}, new int[]{6}, System.out);
        Player player = new Player("Pippo");
        player.setPosition(b.get(61));
        b.bouncedBackMove(player.getPosition().getId(), -3, player);
    }


    @Test(expected=IllegalArgumentException.class)
    public void testMoveIllegalStart() {
        Board b = new Board(new int[]{5,9,14,18,23,27}, new int[]{6}, System.out);
        Player player = new Player("Pippo");
        player.setPosition(b.get(64));
        b.bouncedBackMove(player.getPosition().getId(), 3, player);
    }

}
