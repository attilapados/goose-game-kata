package com.goose;

/**
 * Represents a placeholder on the board where a user can stand. It can have a
 * speciality.
 *
 * @author padisah
 *
 */
public class Block {

    private Integer id;

    private Special special;

    private Board board;

    public Block(Board board, Integer id, Special special) {
        super();
        this.board = board;
        this.id = id;
        this.special = special;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Special getSpecial() {
        return special;
    }

    public void setSpecial(Special special) {
        if (this.special != Special.NORMAL) {
            throw new IllegalArgumentException("No special override allowed!");
        }
        this.special = special;
    }

    public Board getBoard() {
        return board;
    }

    public void setBoard(Board board) {
        this.board = board;
    }

    public String getName() {
        return String.format(special.getName(), Integer.toString(id));
    }

}
