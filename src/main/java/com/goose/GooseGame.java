package com.goose;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import com.goose.util.Dice;

public class GooseGame {
    private static final String ADD_PLAYER =  "add player";

    private static final String MOVE_PLAYER =  "move ";

    private static final String EXIT =  "exit";

    private static final String QUIT =  "quit";

    private static final String HELP =  "help";

    private static final String PLAYERS = "players";

    private static final String PRANK_MESSAGE =  "On %s there is %s, who returns to %s";

    private static final String USER_ALREADY_EXISTS = "%s: already existing player";

    private static final String USER_ROLLS_DICE = "%s rolls %d, %d";

    private static final String WHO_MOVED = "Who has moved!?";

    private static final String PLAYER_LIST = "players: %s";

    private static final String INVALID_MOVE_PARAM = "%s doesn't match the dice roll pattern ex: 2, 3";

    private static final String INVALID_COMMAND = "I don't understand";

    private static final String EXIT_GAME = "Exiting game! Good Bye!";

    private static final String PLAYER_ALREADY_AT_GOAL = "%s is already at the Goal";

    /*Goose game - text based
     *  The spaces 5, 9, 14, 18, 23, 27 have a picture of "The Goose"
     *  5. Space "6" is "The Bridge"
     */
    private PrintStream printer;

    private Scanner scanner;

    public GooseGame(PrintStream printer, Scanner scanner) {
        super();
        this.printer = printer;
        this.scanner = scanner;
    }

    public void play() {

        Board board = new Board(new int[]{5,9,14,18,23,27}, new int[]{6}, printer);
        Map<String, Player> players = new HashMap<String, Player>();
        Dice dice = new Dice();
        boolean running = true;
        printHelp();
        while (running) {
            board.displayBoard(players.values());
            String command = scanner.nextLine();
            // create user
            if (command.startsWith(ADD_PLAYER)) {
                addPlayer(board, players, command);
                printer.println(String.format(PLAYER_LIST, listPlayers(players.values())));
            } else if (command.startsWith(EXIT) || command.startsWith(QUIT)) {
                running = exitGame();
            } else if (command.startsWith(HELP)) {
                printHelp();
            } else if (command.startsWith(PLAYERS)) {
                printPlayers(players.values());
            } else if (command.startsWith(MOVE_PLAYER)) {
                running = movePlayer(board, players, dice, command);
            } else {
                printer.println(INVALID_COMMAND);
            }
        }
        scanner.close();
    }

    public boolean movePlayer(Board board, Map<String, Player> players, Dice dice, String command) {
        String moveParam = command.replaceFirst(MOVE_PLAYER, "").trim();
        if (moveParam.isEmpty()) {
            printer.println(WHO_MOVED);
        } else {
            Player player = null;
            List<String> playerNames = new ArrayList<String>(players.keySet());
            playerNames.sort(new Comparator<String>() {

                @Override
                public int compare(String o1, String o2) {
                    return -1 * Integer.compare(o1.length(), o2.length());
                }

            });

            for (String playerName : playerNames) {
                if (moveParam.startsWith(playerName)) {
                    player = players.get(playerName);
                    break;
                }
            }
            if (player == null) {
                printer.println(WHO_MOVED);
            } else {
                if (player.getPosition().getSpecial() == Special.GOAL) {
                    printer.println(String.format(PLAYER_ALREADY_AT_GOAL, player.getName()));
                } else {
                    int d1 = 0;
                    int d2 = 0;
                    moveParam = moveParam.replaceFirst(player.getName(), "").trim();
                    if (moveParam.isEmpty()) {
                        // auto dice
                        d1 = dice.throwSingleDice();
                        d2 = dice.throwSingleDice();
                    } else {
                        String params[] = moveParam.split(",");
                        if (params.length == 2 && params[0].trim().matches("\\d{1,1}") && params[1].trim().matches("\\d{1,1}")) {
                            try {
                                d1 = Integer.parseInt(params[0].trim());
                                d2 = Integer.parseInt(params[1].trim());
                            } catch (NumberFormatException e) {
                                // will never happen
                            }
                        }
                    }
                    if (d1 > 0 && d1 <= 6 && d2 > 0 && d2 <=6) {
                        player.addMessage(String.format(USER_ROLLS_DICE, player.getName(), d1, d2));
                        Block originalBlock = player.getPosition();
                        board.move(d1 + d2, player);
                        if( player.getPosition().getSpecial() != Special.GOAL && player.getPosition().getSpecial() != Special.START) {
                            for (Player otherPlayer : players.values()) {
                                if (!otherPlayer.equals(player) && otherPlayer.getPosition().equals(player.getPosition())) {
                                    // prank other user
                                    otherPlayer.setPosition(originalBlock);
                                    player.addMessage(String.format(PRANK_MESSAGE,player.getPosition().getName(), otherPlayer.getName(), originalBlock.getName()));
                                    break;
                                }
                            }}
                        printer.println(player.getMessage());

                        if (player.hasWon()) {
                            return false;
                        }
                    } else {
                        printer.println(String.format(INVALID_MOVE_PARAM, moveParam));
                    }}
            }

        }
        return true;
    }

    private boolean exitGame() {
        boolean running;
        running = false;
        printer.println(EXIT_GAME);
        return running;
    }

    private void addPlayer(Board board, Map<String, Player> players, String command) {
        String playerName = command.replaceFirst(ADD_PLAYER, "").trim();
        if (players.containsKey(playerName)) {
            printer.println(String.format(USER_ALREADY_EXISTS, playerName));
        } else {
            Player p = new Player(playerName);
            p.setPosition(board.get(0));
            players.put(p.getName(), p);
        }
    }

    private String listPlayers(Collection<Player> players) {
        StringBuilder sb = new StringBuilder();
        for (Player p : players) {
            if (sb.length() > 0) sb.append(", ");
            sb.append(p.getName());
        }
        return sb.toString();
    }

    private void printPlayers(Collection<Player> players) {
        printer.println("List of players:");
        for (Player p : players) {
            printer.println(String.format("%s is at %s", p.getName(), p.getPosition().getName()));
        }
    }

    private void printHelp() {
        printer.println("Game Commands: ");
        printer.println(HELP + " prints this help");
        printer.println(ADD_PLAYER + " playerName - adds a new player");
        printer.println(EXIT + " exits the game");
        printer.println(QUIT + " exits the game");
        printer.println(PLAYERS + " prints the list of active players");
        printer.println(MOVE_PLAYER + " playerName dice1, dice2 ex: " + MOVE_PLAYER + " Pippo 1,2 - Player Pippo moves by dice thrown manually" );
        printer.println(MOVE_PLAYER + " playerName ex: " + MOVE_PLAYER + " Pippo - Player Pippo moves with auto dice thrown");
    }

}
