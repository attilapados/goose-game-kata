package com.goose;

import java.util.Scanner;

public class App {

    public static void main(String[] args) {
        GooseGame game = new GooseGame(System.out, new Scanner(System.in));
        game.play();
    }

}
