package com.goose;

public class Player {

    private String name;

    private Block position;

    private String message = "";

    private boolean win = false;

    public Player(String name) {
        super();
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Block getPosition() {
        return position;
    }

    public void setPosition(Block position) {
        this.position = position;
    }

    public boolean hasWon() {
        return win;
    }

    public void wins() {
        this.win = true;
    }

    public void addMessage(String m) {
        if (!message.isEmpty()) {
            message += ". ";
        }
        message += m;
    }

    public String getMessage() {
        String m = message;
        message = "";
        return m;
    }

}
