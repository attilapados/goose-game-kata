package com.goose;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/*
 * Holds the - in memory - board data
 * The spaces 5, 9, 14, 18, 23, 27 have a picture of "The Goose"
 * 5. Space "6" is "The Bridge"
 *
 */
public class Board {

    private static final String USER_MOVES = "%s moves from %s to %s";

    private static final String USER_MOVES_AGAIN = "%s moves again and goes to %s";

    private static final String USER_MOVE_BOUNCED = "%s bounces! %s returns to %s";

    private static final String USER_MOVE_WINS = "%s Wins!!";

    private static final String USER_JUMPS_BRIDGE = "%s jumps to %s";

    private static final String USER_DIES_AND_STARTS_OVER = "%s died and goes to %s";

    private static final int BRIDGE_DISTANCE = 6;

    private List<Block> blocks = new ArrayList<Block>();

    private PrintStream printer;

    public Board(int gooeses[], int[] bridges, PrintStream printer) {
        this.printer  = printer;
        for (int i = 0; i < 64; i++) {
            blocks.add(new Block(this, i, Special.NORMAL));
        }
        for (int i = 0; i < gooeses.length; i++) {
            blocks.get(gooeses[i]).setSpecial(Special.GOOSE);
        }

        for (int i = 0; i < bridges.length; i++) {
            blocks.get(bridges[i]).setSpecial(Special.BRIDGE);
        }

        blocks.get(0).setSpecial(Special.START);
        blocks.get(blocks.size() - 1).setSpecial(Special.GOAL);
    }

    public Block get(int i) {
        return blocks.get(i >= blocks.size() ? blocks.size() -1 : i);
    }


    public void move(int dice, Player p) {
        int from = p.getPosition().getId();
        int to = bouncedBackMove(from , dice, p);
        Block target = get(to);
        if (to == from + dice) {
            p.addMessage(String.format(USER_MOVES, p.getName(), p.getPosition().getName(), target.getName() ));
        }
        do {
            switch (target.getSpecial()) {
            case GOOSE : {
                to = bouncedBackMove(to , dice, p);
                p.addMessage(String.format(USER_MOVES_AGAIN, p.getName(), get(to).getName() ));
                break;
            }
            case DEATH: {
                to = 0;
                p.addMessage(String.format(USER_DIES_AND_STARTS_OVER, p.getName(), get(to).getName()));
                break;
            }
            case BRIDGE: {
                to = bouncedBackMove(to , BRIDGE_DISTANCE, p);
                p.addMessage(String.format(USER_JUMPS_BRIDGE, p.getName(), get(to).getName()));
                break;
            }
            case GOAL: {
                to = blocks.size() - 1;
                p.wins();
                p.setPosition(get(to));
                p.addMessage(String.format(USER_MOVE_WINS, p.getName()));
                break;
            }
            default : {
                break;
            }
            }
            target = get(to);
        } while(target.getSpecial() == Special.GOOSE ||  target.getSpecial() == Special.BRIDGE);
        p.setPosition(target);
    }

    /* go round
     */
    public int bouncedBackMove(int from, int steps, Player p) {
        if (steps < 0) {
            throw new IllegalArgumentException("steps must be positive");
        }
        if (from >= blocks.size() -1) {
            throw new IllegalArgumentException("From exceeds board limit");
        }
        int to = from + steps;
        if ( to >= blocks.size()) {
            to = blocks.size() -3;
            p.addMessage(String.format(USER_MOVES, p.getName(), p.getPosition().getName(), blocks.size() - 1));
            p.addMessage(String.format(USER_MOVE_BOUNCED, p.getName(), p.getName(), blocks.get(to).getName()));
        }
        return to;
    }

    public void displayBoard(Collection<Player> players) {
        StringBuilder sb = new StringBuilder();
        int counter = 0;
        for(Block b : blocks) {
            if(sb.length() > 0) sb.append(",");
            if (counter++ % 20 == 0) {
                sb.append("\r\n");
            }
            sb.append(b.getName());
            sb.append("[");
            StringBuilder playerNames = new StringBuilder();
            for (Player p : players) {
                if (p.getPosition().equals(b)) {
                    if (playerNames.length() > 0) playerNames.append(",");
                    playerNames.append(p.getName());
                }
            }
            sb.append(playerNames).append("]");
        }
        printer.println(sb.toString());
    }

    /* go round version
     */
    private int roundMove(int from, int steps, Player p) {
        int to = from + steps;
        if ( to >= blocks.size()) {
            to = to - blocks.size();
        }
        return to;
    }

}
