package com.goose;

/**
 * Special marker of a cell They are mutually exclusive
 *
 * @author padisah
 *
 */
public enum Special {
    START("The Start"),
    NORMAL("%s"),
    GOOSE("%s, The Goose"),
    BRIDGE("The Bridge"),
    DEATH("The Death"),
    GOAL("The Goal");

    private String name;

    private Special(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
