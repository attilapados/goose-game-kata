package com.goose.util;

public class Dice {

    /**
     * Throws a single 6 sided cubic dice
     * @return
     */
    public int throwSingleDice() {
        return 1 + (int) (Math.floor((Math.random() * 6.0f)));
    }

}
